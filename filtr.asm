## Pawel Kubik

.data
ifname:	.asciiz	"/home/maxyan/Workspace/ARKO/in.bmp"
ofname:	.asciiz	"/home/maxyan/Workspace/ARKO/out.bmp"

	.align	2
	
#fil:	.word	-1	-1	-1
#		-1	8	-1
#		-1	-1	-1

fil:	.word	1	1	1
		1	2	1
		1	1	1
		
bufbeg:	.space	4
bufhead:.space	4
pixsize:.space	4

.text	
## # - Normalizacja filtru
	la	$s3, fil	# #
	lw	$t0, 0($s3)	# #
	lw	$t1, 4($s3)	# #
	lw	$t2, 8($s3)	# #
	add	$s4, $t0, $t1	# #	s4 = t0 + t1
	sll	$t0, $t0, 16
	lw	$t3, 12($s3)	# #
	sll	$t1, $t1, 16
	lw	$t4, 16($s3)	# #
	add	$s5, $t2, $t3	# #	s5 = t2 + t3
	sll	$t2, $t2, 16
	lw	$t5, 20($s3)	# #
	sll	$t3, $t3, 16
	lw	$t6, 24($s3)	# #
	add	$s6, $s4, $s5	# #	s6 = t0 + t1 + t2 + t3
	lw	$t7, 28($s3)	# #
	add	$s4, $t4, $t5	# #	s4 = t4 + t5
	sll	$t4, $t4, 16
	lw	$t8, 32($s3)	# #
	sll	$t5, $t5, 16
	add	$s5, $t6, $t7	# #	s5 = t6 + t7
	sll	$t6, $t6, 16
	sll	$t7, $t7, 16
	add	$s6, $s6, $s4	# #	s6 = t0 + t1 + t2 + t3 + t4 + t5
	add	$s5, $s5, $t8	# #	s5 = t6 + t7 + t8
	sll	$t8, $t8, 16
	add	$s6, $s6, $s5	# #	s6 = all
	
	beqz	$s6, normskip
	div	$t0, $s6
	mflo	$t0
	div	$t1, $s6
	mflo	$t1
	div	$t2, $s6
	mflo	$t2
	div	$t3, $s6
	mflo	$t3
	div	$t4, $s6
	mflo	$t4
	div	$t5, $s6
	mflo	$t5
	div	$t6, $s6
	mflo	$t6
	div	$t7, $s6
	mflo	$t7
	div	$t8, $s6
	mflo	$t8
normskip:


## @ - Otwarcie pliku wejsciowego
	li	$v0, 13		# @
	la	$a0, ifname	# @
	li	$a1, 0		# @
	syscall			# @
	ori 	$s3, $v0, 0	# @
	
## Alokacja pamieci na na glowek - poswieca 2 bajty, zeby wyrownac slowa
	li	$v0, 9
	li	$a0, 56
	syscall
	addiu	$v0, $v0, 2
	
## Wczytanie naglowka
	ori	$a1, $v0, 0
	ori 	$a0, $s3, 0
	li 	$v0, 14
	li	$a2, 54
	syscall
	addiu	$t9, $a1, -2
	
	lw	$s6, 20($t9) #width
	addiu	$s6, $s6, 3 #sufit do wielokrotnosci 4
	andi	$s6, $s6, 0xfffffffc 
	sll	$s0, $s6, 1
	sll	$s6, $s6, 3
	subu	$s6, $s6, $s0
		
### Wczytuje zostawiajac dwa wiersze pikseli przed bitmapa
### podczas przetwarzania i-tego wierszu zapisuje wyniki w (i-1)-szym
### -ogranicza operacje na pamieci i odwolania do systemu

## Alokacja pamieci na reszte pliku
	la	$s4, bufbeg 	#-
	la	$s5, bufhead	#&
	la	$s7, pixsize	#+
	lw	$s0, 4($t9)
	lw	$s1, 12($t9)
	subu	$s0, $s0, $s1 #rozmiar danych pikseli
	li 	$v0, 9
	addu	$a0, $s0, $s6
	syscall
	sw	$v0, ($s4)	#-
	sw	$t9, ($s5)	#&
	sw	$s0, ($s7)	#+
	
	
## Wczytanie danych pikseli
	ori	$a0, $s3, 0
	addu	$a1, $v0, $s6
	li	$v0, 14
	ori	$a2, $s5, 0
	syscall
	
## Zamkniecie pliku wejsciowego
	li	$v0, 16
	syscall
	
## Zastosowanie filtru
	##########################Blue
	srl	$s6, $s6, 1	# WIDTH bylo podwojna linia
	ori	$a0, $a1, 0	# a0 - adres bajtu wejsciowego
	subu	$a2, $a1, $s6	# a2 - adres bajtu wyjsciowego

	ori	$t9, $s0, 0	#licznik petli

	lbu	$s0, 0($a0)
	lbu	$s3, 3($a0)
	addu	$a0, $a0, $s6
	addiu	$a2, $a2, 3
	lbu	$s1, 0($a0)
	lbu	$s4, 3($a0)
	addu	$a0, $a0, $s6
	
	
	lbu	$s2, 0($a0)
	lbu	$s5, 3($a0)
	
	###########################################	t0 t1 t2	s0 s3 -
	###########################################	t3 t4 t5	s1 s4 -
	###########################################	t6 t7 t8	s2 s5 -

compBlue:	
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mult	$s0, $t0	#
	ori	$s0, $s3, 0
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mflo	$s7		# s7 = r0
	mult	$s3, $t1	#
	lbu	$s3, 6($a0)
	addu	$a0, $a0, $s6
	
	mflo	$a1		# a1 = r1
	mult	$s1, $t3
	
	ori	$s1, $s4, 0
	
	add	$s7, $s7, $a1	# s7 = r0 + r1
	mflo	$a1		# a1 = r3
	
	mult	$s4, $t4	#
	
	lbu	$s4, 6($a0)
	
	mflo	$v0		# v0 = r4
	mult	$s2, $t6	#
	
	addu	$a0, $a0, $s6
	add	$a1, $a1, $v0	# a1 = r3 + r4
	mflo	$v0		# v0 = r6
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4
	mult	$s5, $t7
	lbu	$s5, 6($a0)
	ori	$s2, $s5, 0
	mflo	$a1		# a1 = r7
	mult	$s3, $t2
	add	$a1, $a1, $v0	# a1 = r6 + r7
	mflo	$v0		# v0 = r2
	mult	$s4, $t5
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4 + r6 + r7
	mflo	$a1		# a1 = r5
	mult	$s5, $t8
	add	$a1, $a1, $v0	# a1 = r5 + r2
	mflo	$v0
	add	$s7, $s7, $a1	# s7 = rALL - r8

	addiu	$t9, $t9, -3	# dec counter
	
	add	$s7, $s7, $v0	# s7 = ALL
	
	sra	$s7, $s7, 16
	ori	$v0, $0, 0xff
	bgez	$s7, nozeroB
	ori	$s7, $0, 0
nozeroB:
	blt	$s7, $v0, nogtffB
	ori	$s7, $0, 0xff
nogtffB:
	sb	$s7, ($a2)	
	addiu	$a0, $a0, 3
	addiu	$a2, $a2, 3
	
	
	bnez	$t9, compBlue
	
	#######################Green
	la	$s7, bufbeg
	la	$t9, pixsize
	lw	$a2, ($s7)
	lw	$t9, ($t9)	#licznik petli
	addiu	$a2, $a2, 1
	addu	$a2, $a2, $s6	# a2 - adres bajtu wyjsciowego
	addu	$a0, $a2, $s6	# a0 - adres bajtu wejsciowego
	
	lbu	$s0, 0($a0)
	lbu	$s3, 3($a0)
	addu	$a0, $a0, $s6
	addiu	$a2, $a2, 3	# nie zaczynac od krawedzi
	lbu	$s1, 0($a0)
	lbu	$s4, 3($a0)
	addu	$a0, $a0, $s6
	
	
	lbu	$s2, 0($a0)
	lbu	$s5, 3($a0)
	
compGreen:	
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mult	$s0, $t0	#
	ori	$s0, $s3, 0
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mflo	$s7		# s7 = r0
	mult	$s3, $t1	#
	lbu	$s3, 6($a0)
	addu	$a0, $a0, $s6
	
	mflo	$a1		# a1 = r1
	mult	$s1, $t3
	
	ori	$s1, $s4, 0
	
	add	$s7, $s7, $a1	# s7 = r0 + r1
	mflo	$a1		# a1 = r3
	
	mult	$s4, $t4	#
	
	lbu	$s4, 6($a0)
	
	mflo	$v0		# v0 = r4
	mult	$s2, $t6	#
	
	addu	$a0, $a0, $s6
	add	$a1, $a1, $v0	# a1 = r3 + r4
	mflo	$v0		# v0 = r6
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4
	mult	$s5, $t7
	lbu	$s5, 6($a0)
	ori	$s2, $s5, 0
	mflo	$a1		# a1 = r7
	mult	$s3, $t2
	add	$a1, $a1, $v0	# a1 = r6 + r7
	mflo	$v0		# v0 = r2
	mult	$s4, $t5
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4 + r6 + r7
	mflo	$a1		# a1 = r5
	mult	$s5, $t8
	add	$a1, $a1, $v0	# a1 = r5 + r2
	mflo	$v0
	add	$s7, $s7, $a1	# s7 = rALL - r8

	addiu	$t9, $t9, -3	# dec counter
	
	add	$s7, $s7, $v0	# s7 = ALL
	
	sra	$s7, $s7, 16
	ori	$v0, $0, 0xff
	bgez	$s7, nozeroG
	ori	$s7, $0, 0
nozeroG:
	blt	$s7, $v0, nogtffG
	ori	$s7, $0, 0xff
nogtffG:
	sb	$s7, ($a2)	
	addiu	$a0, $a0, 3
	addiu	$a2, $a2, 3
	
	bnez	$t9, compGreen
	
	##########################Red
	
	la	$s7, bufbeg
	la	$t9, pixsize
	lw	$a2, ($s7)
	lw	$t9, ($t9)	#licznik petli
	addiu	$a2, $a2, 2
	addu	$a2, $a2, $s6	# a2 - adres bajtu wyjsciowego
	addu	$a0, $a2, $s6	# a0 - adres bajtu wejsciowego
	
	lbu	$s0, 0($a0)
	lbu	$s3, 3($a0)
	addu	$a0, $a0, $s6
	addiu	$a2, $a2, 3	# nie zaczynac od krawedzi
	lbu	$s1, 0($a0)
	lbu	$s4, 3($a0)
	addu	$a0, $a0, $s6
	
	
	lbu	$s2, 0($a0)
	lbu	$s5, 3($a0)
	
compRed:	
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mult	$s0, $t0	#
	ori	$s0, $s3, 0
	subu	$a0, $a0, $s6	##powrot czytnika do gory
	mflo	$s7		# s7 = r0
	mult	$s3, $t1	#
	lbu	$s3, 6($a0)
	addu	$a0, $a0, $s6
	
	mflo	$a1		# a1 = r1
	mult	$s1, $t3
	
	ori	$s1, $s4, 0
	
	add	$s7, $s7, $a1	# s7 = r0 + r1
	mflo	$a1		# a1 = r3
	
	mult	$s4, $t4	#
	
	lbu	$s4, 6($a0)
	
	mflo	$v0		# v0 = r4
	mult	$s2, $t6	#
	
	addu	$a0, $a0, $s6
	add	$a1, $a1, $v0	# a1 = r3 + r4
	mflo	$v0		# v0 = r6
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4
	mult	$s5, $t7
	lbu	$s5, 6($a0)
	ori	$s2, $s5, 0
	mflo	$a1		# a1 = r7
	mult	$s3, $t2
	add	$a1, $a1, $v0	# a1 = r6 + r7
	mflo	$v0		# v0 = r2
	mult	$s4, $t5
	add	$s7, $s7, $a1	# s7 = r0 + r1 + r3 + r4 + r6 + r7
	mflo	$a1		# a1 = r5
	mult	$s5, $t8
	add	$a1, $a1, $v0	# a1 = r5 + r2
	mflo	$v0
	add	$s7, $s7, $a1	# s7 = rALL - r8

	addiu	$t9, $t9, -3	# dec counter
	
	add	$s7, $s7, $v0	# s7 = ALL
	
	sra	$s7, $s7, 16
	ori	$v0, $0, 0xff
	bgez	$s7, nozeroR
	ori	$s7, $0, 0
nozeroR:
	blt	$s7, $v0, nogtffR
	ori	$s7, $0, 0xff
nogtffR:
	sb	$s7, ($a2)	
	addiu	$a0, $a0, 3
	addiu	$a2, $a2, 3
	
	bnez	$t9, compRed
		
	
## Otwarcie pliku wyjsciowego i wypisanie zmodyfikowanego bufora
	li	$v0, 13
	la	$a0, ofname
	li	$a1, 1
	syscall
	
	la	$s0, bufhead
	ori 	$a0, $v0, 0
	lw	$a1, ($s0)
	li	$a2, 54
	addiu	$a1, $a1, 2
	li	$v0, 15
	syscall
	
	la	$s0, bufbeg
	la	$s1, pixsize
	lw	$a1, ($s0)
	lw	$a2, ($s1)
	li	$v0, 15
	syscall
	
	li	$v0, 16
	syscall
	
END:	li $v0,10
	syscall	
	
